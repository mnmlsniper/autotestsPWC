const ProfilePage = function () {
  // fields
  const emailField = '.field.email>.value';
  const phoneField = '.field.phone>.value';
  const usernameField = '.field.username>.value';

  this.getEmail = async function (page) {
    const emailText = await page.textContent(emailField);
    return emailText;
  };

  this.getPhone = async function (page) {
    const phoneText = await page.textContent(phoneField);
    return phoneText;
  };

  this.getUsername = async function (page) {
    const usernameText = await page.textContent(usernameField);
    return usernameText;
  };

  this.pageUrl = async function (page) {
    const pageUrlText = await page.url();
    return pageUrlText;
  };
};

export { ProfilePage };
