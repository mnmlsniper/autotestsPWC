const PublicMainPage = function () {
  // buttons
  const gameLoginButton = '#scrollGamesContainer>div.list>div.grid>div:nth-child(1)>div>div.ask-play.public>table>tbody>tr>td>div>div:nth-child(1)>button>span.auto-GAMES_ASK_PLAY_LOGIN';
  const headerLoginButton = 'button.btn.btn-primary.popup';
  const headerLoginButtonSpan = '.auto-MAIN_MENU_LOGIN';
  const headerRegistrationButton = '.auto-MAIN_MENU_JOIN';
  const pushNotification = '#onesignal-slidedown-allow-button';
  const sliderRegistrationButton = '.auto-MAIN_DOUBLE_DEPOSIT_SUBMIT';

  // fields
  const headerLoginField = 'div.input-wrap.login>input';
  const headerPasswordField = 'div.input-wrap.password>input';

  // icons
  const gameIcon = '#scrollGamesContainer>div.list>div.grid>div:nth-child(1)>div.item';

  this.closePushNotification = async function (page) {
    const pushNotificationElementsHandle = await page.$(pushNotification);
    if (pushNotificationElementsHandle) await page.click(pushNotification);
  };

  this.headerLogin = async function (page, username, password) {
    await page.waitForNavigation();
    await page.fill(headerLoginField, username);
    await page.fill(headerPasswordField, password);
    await page.click(headerLoginButtonSpan);
  };

  this.openModalLoginFromHeader = async function (page) {
    await page.click(headerLoginButton);
  };

  this.openModalLoginFromGame = async function (page) {
    const gameIconElementsHandle = await page.$(gameIcon);
    await gameIconElementsHandle.hover();
    await page.click(gameLoginButton);
  };

  this.openRegistrationModalFromHeader = async function (page) {
    await page.click(headerRegistrationButton);
  };

  this.openRegistrationModalFromSlider = async function (page) {
    await page.click(sliderRegistrationButton);
  };

  this.pageUrl = async function (page) {
    const pageUrlText = await page.url();
    return pageUrlText;
  };
};

export { PublicMainPage };
