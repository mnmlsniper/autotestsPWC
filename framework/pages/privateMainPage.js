const PrivateMainPage = function () {
  // buttons
  const profileButton = 'div.btn-circle.profile';
  const MOCloseButton = 'body > div.fade.modal.show > div > div > div.modal-header > button > span:nth-child(1)';

  // modal windows
  const welcomeBonusMO = 'div.modal-dialog.bonus-offer-modal';
  const welcomeBonusMOCloseButton = 'div.modal-dialog.bonus-offer-modal button.close';

  this.closeMO = async function (page) {
    const MOPresent = await page.isVisible(MOCloseButton);
    if (MOPresent) {
      await page.click(MOCloseButton);
    }
  };

  this.closeWelcomeBonusMO = async function (page) {
    const MOPresent = await page.isVisible(welcomeBonusMO);
    if (MOPresent) {
      await page.click(welcomeBonusMOCloseButton);
    }
  };

  this.goToProfile = async function (page) {
    await page.click(profileButton);
  };

  this.pageUrl = async function (page) {
    const pageUrlText = await page.url();
    return pageUrlText;
  };
};

export { PrivateMainPage };
