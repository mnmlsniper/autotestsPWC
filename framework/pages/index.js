import { PrivateMainPage } from './privateMainPage';
import { ProfilePage } from './profilePage';
import { PublicMainPage } from './publicMainPage';

const app = (page) => ({
  PrivateMainPage: () => new PrivateMainPage(page),
  ProfilePage: () => new ProfilePage(page),
  PublicMainPage: () => new PublicMainPage(page),
});

export { app };
