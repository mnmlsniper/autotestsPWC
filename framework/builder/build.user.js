const BuildUser = function () {
  this.createLogin = function (currentMoment) {
    const login = `inna${currentMoment}`;
    return login;
  };
  this.createEmail = function (currentMoment) {
    const email = `inna${currentMoment}@mail.ru`;
    return email;
  };
  this.createPhone = function (currentMoment) {
    const phone = `+7910${currentMoment}`;
    return phone;
  };
  this.createPhoneForCall = function (currentMoment) {
    const phone = `+44745${currentMoment}`;
    return phone;
  };
};

export { BuildUser };
