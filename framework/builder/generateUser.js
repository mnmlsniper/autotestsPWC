import { apiProvider } from '../services';
import { BuildUser } from './build.user';
import { GenerateCurrentMoment } from '../../lib/helpers/generateCurrentMoment';
import { GenerateRandom } from '../../lib/helpers';

const GenerateUser = function () {
  this.generateUserWithEmail = async function () {
    const currentMoment = new GenerateCurrentMoment().generateCurrentMoment();
    const r = await apiProvider().GetCurrencies().post();
    const i = new GenerateRandom().randomInteger(0, r.body.data.length - 1);
    const user = {
      nickName: new BuildUser().createLogin(currentMoment),
      phoneOrEmail: new BuildUser().createEmail(currentMoment),
      currencyCode: r.body.data[i].currencyCode,
      password: 'raya16',
      subscribeOnNews: false,
      language: 'fr',
      captchaResponse: '',
      dealerCode: '',
    };
    return user;
  };
  this.generateUserWithPhone = async function () {
    const currentMoment = new GenerateCurrentMoment().generateCurrentMoment();
    const r = await apiProvider().GetCurrencies().post();
    const i = new GenerateRandom().randomInteger(0, r.body.data.length - 1);
    const user = {
      nickName: new BuildUser().createLogin(currentMoment),
      phoneOrEmail: new BuildUser().createPhone(currentMoment),
      currencyCode: r.body.data[i].currencyCode,
      password: 'raya16',
      subscribeOnNews: false,
      language: 'en',
      captchaResponse: '',
      dealerCode: '',
    };
    return user;
  };
  this.generateUserWithPhoneForCall = async function () {
    const currentMoment = new GenerateCurrentMoment().generateCurrentMoment();
    const r = await apiProvider().GetCurrencies().post();
    const i = new GenerateRandom().randomInteger(0, r.body.data.length - 1);
    const user = {
      nickName: new BuildUser().createLogin(currentMoment),
      phoneOrEmail: new BuildUser().createPhoneForCall(currentMoment),
      currencyCode: r.body.data[i].currencyCode,
      password: 'raya16',
      subscribeOnNews: false,
      language: 'en',
      captchaResponse: '',
      dealerCode: '',
    };
    return user;
  };
};

export { GenerateUser };
