const MOsteps = function () {
  // buttons
  const callMeButton = '.auto-MAIN_RECALL_TEXT';

  // fields
  const confirmationCodeField = 'input[name=emailCode]';
  const confirmationCodeSMSField = 'input[name=smsCode]';

  this.clickCallMeButton = async function (page) {
    await page.click(callMeButton);
  };

  this.fillEmailCode = async function (page, confCode) {
    await page.fill(confirmationCodeField, confCode);
  };

  this.fillSMScode = async function (page, confCode) {
    await page.fill(confirmationCodeSMSField, confCode);
  };
};

export { MOsteps };
