import { MOsteps } from './onModals';

const step = (page) => ({
  MOsteps: () => new MOsteps(page),
});

export { step };
