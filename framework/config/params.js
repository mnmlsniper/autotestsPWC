const usersForAuth = {
  blockedUser: {
    phoneOrEmail: 'inna1503212@mail.ru',
    password: 'raya16',
    captchaResponse: '',
  },
  nonexistentUser: {
    phoneOrEmail: 'inn1503219@mail.ru',
    password: 'raya16',
    captchaResponse: '',
  },
  userWithEmail: {
    nickName: 'inna1803213',
    phoneOrEmail: 'Inna1803213@mail.ru',
    password: 'raya16',
    captchaResponse: '',
  },
  withPhone: {
    nickName: 'inna0904526',
    phoneOrEmail: '+79100904526',
    password: 'raya16',
    timezone: 'Europe/Moscow',
  },
  resetPassword: {
    nickName: 'inna1803214',
    phoneOrEmail: 'inna0502210@mail.ru',
    password: 'newPassword',
    captchaResponse: '',
  },
  resetPasswordCallMe: {
    nickName: 'inna0904858',
    phoneOrEmail: '+447450904858',
    password: 'newPassword',
    captchaResponse: '',
  },
  resetPasswordPhone: {
    nickName: 'inna2105217',
    phoneOrEmail: '+79102105217',
    password: 'newPassword',
    captchaResponse: '',
  },
};

export { usersForAuth };
