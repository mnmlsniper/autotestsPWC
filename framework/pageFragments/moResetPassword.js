import { step } from '../stepObjects';

const MOResetPassword = function () {
  // buttons
  const confirmResetPasswordButton = 'input[type=checkbox]~button';
  const confirmResetPasswordSMSButton = '#react-app div.custom-modal-body > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > button:nth-child(2)';
  const resetPasswordButton = '.auto-MAIN_RESET_PASSWORD';

  // fields
  const modalResetPasswordLoginField = 'div.custom-modal-body input[name=email]';
  const modalResetPasswordNewPasswordField = '.custom-modal-body input[type=password][name=password]';
  const modalResetPasswordConfirmNewPasswordField = '.custom-modal-body input[type=password][name=confirmPassword]';

  // messages
  const resetPasswordMessage = '.auto-MAIN_CONFIRMATION_CODE_HAS_BEEN_SENT_YOU_BY_EMAIL_TEXT';

  this.getResetPasswordModalMessage = async function (page) {
    const messageText = await page.textContent(resetPasswordMessage);
    return messageText;
  };

  this.confirmEmail = async function (page, confCode, newPassword) {
    await step().MOsteps().fillEmailCode(page, confCode);
    await page.fill(modalResetPasswordNewPasswordField, newPassword);
    await page.fill(modalResetPasswordConfirmNewPasswordField, newPassword);
    await page.click(confirmResetPasswordButton);
  };

  this.confirmPhone = async function (page, confCode, newPassword) {
    await step().MOsteps().fillSMScode(page, confCode);
    await page.fill(modalResetPasswordNewPasswordField, newPassword);
    await page.fill(modalResetPasswordConfirmNewPasswordField, newPassword);
    await page.click(confirmResetPasswordSMSButton);
  };

  this.start = async function (page, username) {
    await page.fill(modalResetPasswordLoginField, username);
    await page.click(resetPasswordButton);
  };
};

export { MOResetPassword };
