const MOLogin = function () {
  // buttons
  const mainLoginButton = '.auto-MAIN_LOGIN';

  // fields
  const modalLoginField = '.custom-modal-login div.input-wrap.login>input';
  const modalPasswordField = '.custom-modal-login div.input-wrap.password>input';

  // links
  const registrationLink = '.auto-MAIN_REGISTER_NEW_ACCOUNT';
  const resetPasswordLink = '.auto-MAIN_FORGOT_PASSWORD';

  this.modalLogin = async function (page, username, password) {
    await page.fill(modalLoginField, username);
    await page.fill(modalPasswordField, password);
    await page.click(mainLoginButton);
  };

  this.openRegistrationModalFromLogin = async function (page) {
    await page.click(registrationLink);
  };

  this.openResetPasswordModal = async function (page) {
    await page.click(resetPasswordLink);
  };
};

export { MOLogin };
