import { step } from '../stepObjects';

const MORegister = function () {
  // buttons
  const headerLoginCreateButton = '.custom-container-account-language button.btn.btn-primary.submit';
  const registerCreateAccountButton = 'div.custom-account-details-button> input + button';
  const registerConfirmCodeButton = 'div.custom-modal-register-account div.custom-account-phone-footer button:nth-child(2)';

  // fields
  const modalRegisterEmailOrPhoneField = '.custom-registration-menu input[name=emailOrPhone]';
  const modalRegisterLoginField = '.custom-registration-menu input[name=userName]';
  const modalRegisterPasswordField = '.custom-registration-menu input[name=password]';

  // selects
  const selectCurrencyForRegistration = '.Select-arrow';
  // const selectEURFromList = 'input[aria-activedescendant=react-select-6--option-0]';
  const selectPLNFromList = '#react-select-2--option-1';
  const selectUSDFromList = '#react-select-2--option-2';

  // tickbox
  const checkboxPrPol = '#lang-ch-1~label';
  const checkboxSubscribe = '#lang-ch-2~label';

  this.fillRegistrationFirstStep = async function (page, username, emailOrPhone, password) {
    await page.fill(modalRegisterLoginField, username);
    await page.fill(modalRegisterEmailOrPhoneField, emailOrPhone);
    await page.fill(modalRegisterPasswordField, password);
    await page.click(modalRegisterLoginField);
    await page.click(registerCreateAccountButton);
  };

  this.fillRegistrationSecondStep = async function (page, currency, subscribe) {
    await page.click(selectCurrencyForRegistration);
    await page.click(selectPLNFromList);
    await page.click(checkboxPrPol);
    if (subscribe === 'subscribe') await page.click(checkboxSubscribe);
    await page.click(headerLoginCreateButton);
  };

  this.fillRegistrationThirdStep = async function (page, confCode) {
    await step().MOsteps().fillSMScode(page, confCode);
    await page.click(registerConfirmCodeButton);
  };
};

export { MORegister };
