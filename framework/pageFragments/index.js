import { MOLogin } from './moLogin';
import { MORegister } from './moRegister';
import { MOResetPassword } from './moResetPassword';

const mo = (page) => ({
  MOLogin: () => new MOLogin(page),
  MORegister: () => new MORegister(page),
  MOResetPassword: () => new MOResetPassword(page),
});

export { mo };
