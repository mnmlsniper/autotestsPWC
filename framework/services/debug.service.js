import supertest from 'supertest';
import { headers } from '../config/headers';
import { urls } from '../config/urls';

const GetConfirmationLog = function GetConfirmationLog() {
  this.get = async function getConfirmationLog() {
    const r = await supertest(urls.testPWC)
      .get('/debug/confirmation-log')
      .set(headers);
    return r;
  };
};
export { GetConfirmationLog };
