import supertest from 'supertest';
import { headers } from '../config/headers';
import { urls } from '../config/urls';

const PostRequest = async function PostRequest(path, params) {
  const r = await supertest(urls.testPWC)
    .post(path)
    .set(headers)
    .send(params);
  return r;
};

const ConfirmEmailAndLogin = function ConfirmEmailAndLogin() {
  this.post = async function ConfirmEmailAndLogin(params) {
    const r = await PostRequest('/api/auth/confirmEmailAndLogin', params);
    return r;
  };
};
export { ConfirmEmailAndLogin };

const ConfirmPhone = function ConfirmPhone() {
  this.post = async function confirmPhone(params) {
    const r = await PostRequest('/api/auth/confirmPhone', params);
    return r;
  };
};
export { ConfirmPhone };

const GetCurrencies = function GetCurrencies() {
  this.post = async function getCurrencies() {
    const r = await supertest(urls.testPWC)
      .post('/api/auth/getCurrencies');
    return r;
  };
};
export { GetCurrencies };

const Login = function Login() {
  this.post = async function login(params) {
    const r = await PostRequest('/api/auth/login', params);
    return r;
  };
};
export { Login };

const Register = function Register() {
  this.post = async function register(params) {
    const r = await PostRequest('/api/auth/register', params);
    return r;
  };
};
export { Register };
