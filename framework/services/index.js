import { GetConfirmationLog } from './debug.service';
import {
  ConfirmEmailAndLogin, ConfirmPhone, GetCurrencies,
  Login, Register,
} from './auth.service';

const apiProvider = () => ({
  ConfirmEmailAndLogin: () => new ConfirmEmailAndLogin(),
  ConfirmPhone: () => new ConfirmPhone(),
  GetConfirmationLog: () => new GetConfirmationLog(),
  GetCurrencies: () => new GetCurrencies(),
  Login: () => new Login(),
  Register: () => new Register(),
});

export { apiProvider };
