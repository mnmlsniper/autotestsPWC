import { GetConfirmationCodes } from './confirmationLogParsing';

export * from './generate.random';

const testDataProvider = () => ({
  GetConfirmationCodes: () => new GetConfirmationCodes(),
});

export { testDataProvider };
