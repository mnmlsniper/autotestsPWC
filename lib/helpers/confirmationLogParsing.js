import { urls } from '../../framework/config';

const GetConfirmationCodes = function GetConfirmationCodes() {
  this.lastEmailCode = function getLastEmailCode(confLogPageSource) {
    const res = JSON.parse(confLogPageSource.text);
    const confCode = res.confirmations[0].code;
    return confCode;
  };
  this.lastSMSCode = function getLastSMSCode(confLogPageSource) {
    const res = JSON.parse(confLogPageSource.text);
    const confCode = res.confirmations[0].code;
    return confCode;
  };
  this.registrationLink = function getRegistrationLink(confLogPageSource) {
    const res = JSON.parse(confLogPageSource.text);
    const pId = res.confirmations[0].idPlayer;
    const { code } = res.confirmations[0];
    const confLink = `${urls.testPWC}en/confirm-email?pId=${pId}&code=${code}`;
    return confLink;
  };
  this.getCodeFromRegistrationLink = function getCodeFromRegistrationLink(confLogPageSource) {
    const res = JSON.parse(confLogPageSource.text);
    const confCode = res.confirmations[0].code;
    return confCode;
  };
};
export { GetConfirmationCodes };
