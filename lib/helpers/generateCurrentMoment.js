const GenerateCurrentMoment = function () {
  this.generateCurrentMoment = function () {
    const date = new Date();
    const currentDate = `${date.getUTCDate()}`;
    const currentDateRes = currentDate.length === 2 ? currentDate : `0${currentDate}`;
    const currentMonth = `${date.getUTCMonth()}`;
    const currentMonthRes = currentMonth.length === 2 ? currentMonth : `0${currentMonth}`;
    const currentMilliseconds = `${date.getUTCMilliseconds()}`;
    let currentMillisecondsRes;
    switch (currentMilliseconds.length) {
      case (1): currentMillisecondsRes = `00${currentMilliseconds}`;
        break;
      case (2): currentMillisecondsRes = `0${currentMilliseconds}`;
        break;
      case (3): currentMillisecondsRes = currentMilliseconds;
        break;
      default: currentMillisecondsRes = '000';
    }
    const currentTime = `${currentDateRes}${currentMonthRes}${currentMillisecondsRes}`;
    return currentTime;
  };
};

export { GenerateCurrentMoment };
