import playwright from 'playwright';

let browser;
let context;
let page;

async function gotoURL(url) {
  await page.goto(url);
  return page;
}

async function runExtraBrowser() {
  browser = await playwright.chromium.launch({
    // Оторбражаем браузер или нет
    headless: true,
    // Эмуляция действий пользователя. Задержка. Время в милисекундах
    // slowMo: 1000,
    // В опциях браузера также задается язык браузера, разрешение на геолокацию,
  });
  // в браузере создаем контекст
  context = await browser.newContext({
    locale: 'en-GB',
  });

  page = await context.newPage();

  // Задать разрешение
  // await page.setViewportSize({
  //   width: 1280,
  //   height: 720,
  // });
}

async function stopExtraBrowser() {
  // Сделать скриншот страницы
  await page.screenshot({ path: 'endPointExtra.jpg' });
  await page.close();
  await browser.close();
}

export
{
  gotoURL, runExtraBrowser, stopExtraBrowser,
};
