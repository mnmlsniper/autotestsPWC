import playwright from 'playwright';
import { urls } from '../../framework/config/urls';

let browser;
let context;
let page;

async function goto() {
  await page.goto(urls.testPWC);
  await page.screenshot({ path: 'endPoint.jpg' });
  return page;
}

async function run() {
  browser = await playwright.chromium.launch({
    // Оторбражаем браузер или нет
    headless: true,
    args: ['--no-sandbox', '--disable-dev-shm-usage', '--use-fake-ui-for-media-stream'],
    // Эмуляция действий пользователя. Задержка. Время в милисекундах
    // slowMo: 1000,
    // В опциях браузера также задается язык браузера, разрешение на геолокацию,
  });
  // в браузере создаем контекст
  context = await browser.newContext({
  //  locale: 'en-GB',
  });

  page = await context.newPage();

  //Задать разрешение
  await page.setViewportSize({
    width: 640,
    height: 480,
  });

  // хочется фильтровать запросы по URL и писать результат не в консоль, а в файл
  // page.on('request', (request) => console.log('>>', request.method(), request.url()));
  // page.on('response', (response) => console.log('<<', response.status(), response.url()));
}

async function stop() {
  // Сделать скриншот страницы
 // await page.screenshot({ path: 'endPoint.jpg' });
  await page.close();
  await browser.close();
}

export
{
  goto, run, stop,
};
