module.exports = {
  testEnvironment: 'node',
  setupFilesAfterEnv: ['./jest.setup.js'],
  reporters: [
    'default',
    'jest-html-reporters',
    'htmlreport4jest',
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/**/profileUI.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
