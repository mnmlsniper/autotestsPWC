import {
  afterEach, beforeEach, describe, expect, test,
} from '@jest/globals';
import {
  goto, run, stop,
} from '../../lib/browser/index';
import { apiProvider } from '../../framework/services';
import { app } from '../../framework/pages';
import { GenerateUser } from '../../framework/builder';
import { mo } from '../../framework/pageFragments';
import { step } from '../../framework/stepObjects';
import { testDataProvider } from '../../lib/helpers';
import { urls, usersForAuth } from '../../framework/config';

describe('PWC login', () => {
  let page;

  beforeEach(async () => {
    await run();
    page = await goto();
    await page.waitForTimeout(3000);
    await app().PublicMainPage().closePushNotification(page);
  });

  afterEach(async () => {
    await stop();
  });

  test.only('User can login with username via header fields', async () => {
    await app().PublicMainPage().headerLogin(page, usersForAuth.withPhone.nickName, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);
    const usernameText = await app().ProfilePage().getUsername(page);
    expect(usernameText)
      .toBe(usersForAuth.withPhone.nickName);
  });

  test('User can login with phone via header fields', async () => {
    await app().PublicMainPage().headerLogin(page, usersForAuth.withPhone.phoneOrEmail, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);
    const phoneText = await app().ProfilePage().getPhone(page);
    expect(phoneText)
      .toBe(usersForAuth.withPhone.phoneOrEmail);
  });

  test('User can login with username via header login button', async () => {
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().modalLogin(page, usersForAuth.withPhone.nickName, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);

    const usernameText = await app().ProfilePage().getUsername(page);
    expect(usernameText)
      .toBe(usersForAuth.withPhone.nickName);
  });

  test('User can login with phone via header login button', async () => {
    await app().PublicMainPage().openModalLoginFromGame(page);
    await mo().MOLogin().modalLogin(page, usersForAuth.withPhone.phoneOrEmail, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);

    const phoneText = await app().ProfilePage().getPhone(page);
    expect(phoneText)
      .toBe(usersForAuth.withPhone.phoneOrEmail);
  });

  test('User can login with username via game login button', async () => {
    await app().PublicMainPage().openModalLoginFromGame(page);
    await mo().MOLogin().modalLogin(page, usersForAuth.withPhone.nickName, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);

    const usernameText = await app().ProfilePage().getUsername(page);
    expect(usernameText)
      .toBe(usersForAuth.withPhone.nickName);
  });

  test('User can login with phone via game login button', async () => {
    await app().PublicMainPage().openModalLoginFromGame(page);
    await mo().MOLogin().modalLogin(page, usersForAuth.withPhone.phoneOrEmail, usersForAuth.withPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);

    const phoneText = await app().ProfilePage().getPhone(page);
    expect(phoneText)
      .toBe(usersForAuth.withPhone.phoneOrEmail);
  });
});

describe('PWC reset password', () => {
  let page;
  // let confLogPage;

  beforeEach(async () => {
    page = await run();
    page = await goto();
    await page.waitForTimeout(3000);
    await app().PublicMainPage().closePushNotification(page);
    // confLogPage = await runExtraBrowser();
    // confLogPage = await gotoURL(`${urls.testPWC}debug/confirmation-log`);
  });

  afterEach(async () => {
    // await stopExtraBrowser();
    await stop();
  });

  test('User can reset password with email', async () => {
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().openResetPasswordModal(page);
    await mo().MOResetPassword().start(page, usersForAuth.resetPassword.phoneOrEmail);
    await page.waitForTimeout(3000);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastEmailCode(r1);
    await mo().MOResetPassword().confirmEmail(page, confCode, usersForAuth.resetPassword.password);
    // перенести ожидания на уровень page object
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    const pageURL = await app().PublicMainPage().pageUrl(page);
    expect(pageURL)
      .toContain('login/reset-password-by-email-complete');
    const r = await apiProvider().Login().post(usersForAuth.resetPassword);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.email).toEqual(usersForAuth.resetPassword.phoneOrEmail);
  });

  test('User can reset password with phone', async () => {
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().openResetPasswordModal(page);
    await mo().MOResetPassword().start(page, usersForAuth.resetPasswordPhone.phoneOrEmail);
    await page.waitForTimeout(3000);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastSMSCode(r1);
    await mo().MOResetPassword().confirmPhone(page, confCode, usersForAuth.resetPasswordPhone.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    const pageURL = await app().PublicMainPage().pageUrl(page);
    expect(pageURL)
      .toContain('login/reset-password-by-sms');
    const r = await apiProvider().Login().post(usersForAuth.resetPasswordPhone);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.phone).toEqual(usersForAuth.resetPasswordPhone.phoneOrEmail);
  });

  test('User can reset password with "call me" button', async () => {
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().openResetPasswordModal(page);
    await mo().MOResetPassword().start(page, usersForAuth.resetPasswordCallMe.phoneOrEmail);
    await step().MOsteps().clickCallMeButton(page);
    await page.waitForTimeout(3000);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastSMSCode(r1);
    await mo().MOResetPassword().confirmPhone(page, confCode, usersForAuth.resetPasswordCallMe.password);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    const pageURL = await app().PublicMainPage().pageUrl(page);
    expect(pageURL)
      .toContain('login/reset-password-by-sms');
    const r = await apiProvider().Login().post(usersForAuth.resetPasswordCallMe);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.phone).toEqual(usersForAuth.resetPasswordCallMe.phoneOrEmail);
  });
});

describe('PWC registration', () => {
  let page;

  beforeEach(async () => {
    page = await run();
    page = await goto();
    await page.waitForTimeout(3000);
    await app().PublicMainPage().closePushNotification(page);
  });

  afterEach(async () => {
    await stop();
  });

  test('User can register with phone', async () => {
    const user = await new GenerateUser().generateUserWithPhone();
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().openRegistrationModalFromLogin(page);
    await mo().MORegister().fillRegistrationFirstStep(page, user.nickName, user.phoneOrEmail, user.password);
    await mo().MORegister().fillRegistrationSecondStep(page, user.currencyCode, 'subscribe');
    await page.waitForTimeout(3000);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastSMSCode(r1);
    await mo().MORegister().fillRegistrationThirdStep(page, confCode);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().goToProfile(page);
    const phoneText = await app().ProfilePage().getPhone(page);
    expect(phoneText)
      .toBe(user.phoneOrEmail);
  });

  test('User can register with email', async () => {
    const user = await new GenerateUser().generateUserWithEmail();
    await app().PublicMainPage().openModalLoginFromHeader(page);
    await mo().MOLogin().openRegistrationModalFromLogin(page);
    await mo().MORegister().fillRegistrationFirstStep(page, user.nickName, user.phoneOrEmail, user.password);
    await mo().MORegister().fillRegistrationSecondStep(page, user.currencyCode, 'subscribe');
    await page.waitForTimeout(3000);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const registrationLink = testDataProvider().GetConfirmationCodes().registrationLink(r1);
    await page.goto(registrationLink);
    await page.waitForTimeout(3000);
    await app().PrivateMainPage().closeWelcomeBonusMO(page);
    await app().PrivateMainPage().closeMO(page);
    await app().PrivateMainPage().goToProfile(page);
    const phoneText = await app().ProfilePage().getEmail(page);
    expect(phoneText)
      .toBe(user.phoneOrEmail);
  });

  test('User can register with phone using "call me" button', async () => {
    const user = await new GenerateUser().generateUserWithPhoneForCall();
    await app().PublicMainPage().openRegistrationModalFromHeader(page);
    await mo().MORegister().fillRegistrationFirstStep(page, user.nickName, user.phoneOrEmail, user.password);
    await mo().MORegister().fillRegistrationSecondStep(page, user.currencyCode, 'subscribe');
    await step().MOsteps().clickCallMeButton(page);

    const response = await page.waitForResponse(`${urls.testPWC}api/auth/resendPhoneConfirmation`);
    const result = await response.json();
    expect(result.success)
      .toBe(true);
    expect(result.errorMessage)
      .toBe(null);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastSMSCode(r1);
    await mo().MORegister().fillRegistrationThirdStep(page, confCode);

    // вынести 2 строчки в step object: newly authorized
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await app().PrivateMainPage().closeWelcomeBonusMO(page);

    // вынести в step object: get account info
    await app().PrivateMainPage().goToProfile(page);
    const phoneText = await app().ProfilePage().getPhone(page);

    expect(phoneText)
      .toBe(user.phoneOrEmail);
  });
});
