import { expect, test } from '@jest/globals';
import { apiProvider } from '../../framework/services/index';
import { GenerateUser } from '../../framework/builder/generateUser';
import { testDataProvider } from '../../lib/helpers';
import { usersForAuth } from '../../framework/config';

// сделать параметризованные негативные тесты на - String errorMessage – это текст серверной ошибки, которую надо отобразить
// пользователю. Это поле заполняется только если success не null.
// - String errorCode - код ошибки из перечня
//  INTERNAL_ERROR - внутренняя ошибка, нужно отобразить текст пользователю
//  INVALID_CURRENCY - не доступная валюта
//  ACCESS_IS_DENIED - доступ запещен, нужно отправить пользователя на форму
// логина
//  PLAYER_OUT_OF_CREDIT - нет достаточно денег на балансе, для выполнения операции
//  PLAYER_ALREADY_EXISTS - игрок с таким email или phone уже зарегистрирован

describe('Авторизация в системе по АПИ', () => {
  test('Пользователь может авторизоваться в системе используя никнейм и пароль', async () => {
    const r = await apiProvider().Login().post(usersForAuth.withPhone);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.nickName).toEqual(usersForAuth.withPhone.nickName);
  });

  test('Пользователь может авторизоваться в системе используя телефон и пароль', async () => {
    const r = await apiProvider().Login().post(usersForAuth.withPhone);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.phone).toEqual(usersForAuth.withPhone.phoneOrEmail);
  });

  test('Пользователь может авторизоваться в системе используя email и пароль', async () => {
    const r = await apiProvider().Login().post(usersForAuth.userWithEmail);
    // console.log(r.body.data.authToken);
    expect(r.status).toEqual(200);
    expect(r.body.data.playerInfo.email).toEqual(usersForAuth.userWithEmail.phoneOrEmail);
  });

  // негативные тесты, которые нужно отрефакторить в параметризованные
  test('Заблокированный пользователь не может авторизоваться в системе используя email и пароль', async () => {
    const r = await apiProvider().Login().post(usersForAuth.blockedUser);
    expect(r.status).toEqual(200);
    expect(r.body.data.blocked).toEqual(true);
  });

  test('Пользователь не может авторизоваться в системе используя неверный email', async () => {
    const r = await apiProvider().Login().post(usersForAuth.nonexistentUser);
    expect(r.status).toEqual(200);
    expect(r.body.data.success).toEqual(false);
    expect(r.body.data.emailConfirmationRequired).toEqual(false);
  });
});

describe('Регистрация в системе по АПИ', () => {
  test('Пользователь может запросить список доступных для регистрации валют', async () => {
    const r = await apiProvider().GetCurrencies().post();
    expect(r.status).toEqual(200);
    expect(r.body.data[0].currencyCode).toEqual('EUR');
  });

  test.skip('Пользователь может зарегистрироваться в системе по номеру телефона', async () => {
    const user = await new GenerateUser().generateUserWithPhone();

    const r = await apiProvider().Register().post(user);
    expect(r.status).toEqual(200);
    expect(r.body.data.smsWasSent).toBeTruthy();

    const r1 = await apiProvider().GetConfirmationLog().get();
    const confCode = testDataProvider().GetConfirmationCodes().lastSMSCode(r1);
    const conf = {
      playerId: r.body.data.playerId,
      code: confCode,
    };
    const r2 = await apiProvider().ConfirmPhone().post(conf);
    expect(r2.status).toEqual(200);
    expect(r2.body.success).toBeTruthy();
  });

  test.skip('Пользователь может зарегистрироваться в системе по email', async () => {
    const user = await new GenerateUser().generateUserWithEmail();

    const r = await apiProvider().Register().post(user);
    expect(r.status).toEqual(200);
    expect(r.body.data.emailWasSent).toEqual(true);
    const r1 = await apiProvider().GetConfirmationLog().get();
    const confLink = testDataProvider().GetConfirmationCodes().getCodeFromRegistrationLink(r1);
    const conf = {
      playerId: r.body.data.playerId,
      code: confLink,
    };
    const r2 = await apiProvider().ConfirmEmailAndLogin().post(conf);
    expect(r2.status).toEqual(200);
    expect(r2.body.success).toBeTruthy();
    expect(r2.body.data.authToken).toBeDefined();
    expect(r2.body.data.showFirstDeposit).toBeTruthy();
  });

  // негативные сценарии, переделать на параметризованные тесты
  test.skip('Пользователь, не завершивший регистрацию, может перейти от логина к верификации email', async () => {
    const user = await new GenerateUser().generateUserWithEmail();
    await apiProvider().Register().post(user);
    const r = await apiProvider().Login().post(user);
    expect(r.status).toEqual(200);
    expect(r.body.data.success).toEqual(false);
    expect(r.body.data.emailConfirmationRequired).toEqual(true);
  });

  test.skip('Пользователь, не завершивший регистрацию, может перейти от логина к верификации телефона', async () => {
    const user = await new GenerateUser().generateUserWithPhone();
    await apiProvider().Register().post(user);
    const r = await apiProvider().Login().post(user);
    expect(r.status).toEqual(200);
    expect(r.body.data.success).toEqual(false);
    expect(r.body.data.phoneConfirmationRequired).toEqual(true);
  });
});
